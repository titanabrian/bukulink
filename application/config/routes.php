<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['home']='home/dashboard';
$route['register']='login/register';
$route['register/process']='login/registerprocess';
$route['data/datalengkap']='MenampilkanData/DataLengkap';
$route['data/datadosen']='MenampilkanData/DataDosen';
$route['data/dataalumni']='MenampilkanData/DataAlumni';
$route['data/datamahasiswa']='MenampilkanData/DataMahasiswa';
$route['data/datalainnya']='MenampilkanData/DataLainnya';
$route['editprofile/(:num)']='MengelolaAkun/vieweditprofile/$1';
$route['editprofile/process']='MengelolaAkun/processeditprofile';
$route['editpassword/process']='MengelolaAkun/editpassword';
$route['data/datapengguna']='MenampilkanData/datapengguna';
$route['data/datapengguna/terima']='MengelolaAkun/terimapengguna';
$route['data/datapengguna/batal']='MengelolaAkun/batalkanpengguna';
$route['data/datapengguna/hapus']='MengelolaAkun/hapuspengguna';
$route['data/datapengguna/jadikanadmin']='MengelolaAkun/jadiadmin';
$route['data/importdata']='MengelolaData/viewimport';
$route['data/importdata/process']='MengelolaData/importdata';
$route['data/datapengguna/fetchdata']='MenampilkanData/fetchdata';
