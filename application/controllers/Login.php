<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  class Login extends CI_Controller{
    public function __construct(){
      parent::__construct();
      if($this->session->userdata('masuk_bukulink')){
        header('Location:'.base_url().'home');
      }
      $this->load->model('user');
      $this->load->model('link');
    }

    public function index(){
      $this->load->view('login');
    }

    public function register(){
      $this->load->view('register');
    }

    public function registerProcess(){
      $this->form_validation->set_error_delimiters('<p class="text text-danger">','</p>');
      $this->form_validation->set_rules('registerUsername','Username','required|min_length[5]|max_length[25]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
      $this->form_validation->set_rules('registerNomorInduk','Nomor Induk','required|numeric|min_length[7]|max_length[20]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
      // $this->form_validation->set_rules('registerNama','Nama','required|min_length[3]|alpha|max_length[30]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
      $this->form_validation->set_rules('registerEmail','Email','required|valid_email|min_length[5]|max_length[30]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
      $this->form_validation->set_rules('registerPassword','Password','required|min_length[8]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));

      if($this->form_validation->run()==FALSE){
        $this->session->set_flashdata(array('validation_errors'=>validation_errors()));
        header('Location:'.base_url().'register');
      }else{
        $username=$this->input->post('registerUsername',TRUE);
        $nomorinduk=$this->input->post('registerNomorInduk',TRUE);
        $email=$this->input->post('registerEmail',TRUE);
        $password=$this->input->post('registerPassword',TRUE);
        $password=md5('pass'.$password.'word');
        // die($password);
        $link = $this->link->getDataByNomorInduk($nomorinduk);
        // die(print_r($link));
        if($link!=false){
          $user= $this->user->getUserByNomorInduk($nomorinduk);
          // die(print_r($user));
          if($user!=false){
            $response['message_register']="<div class='alert alert-danger'><p>Gagal Mendaftar</p></div>";
            $this->session->set_flashdata($response);
            header('Location:'.base_url().'login');
          }else{
            $insert=$this->user->insert($username,$password,$email,$nomorinduk,0);
            if($insert){
              $response['message_register']="<div class='alert alert-success'><p>Berhasil Mendaftar</p></div>";
              $this->session->set_flashdata($response);
              // echo "<script>alert('Berhasil Mendaftar')</script>";
              header('Location:'.base_url().'login');
            }else{
              $response['message_register']="<div class='alert alert-danger'><p>Gagal Mendaftar</p></div>";
              $this->session->set_flashdata($response);
              header('Location:'.base_url().'register');
            }
          }
        }else{
          $response['message_register']="<div class='alert alert-danger'><p>Gagal Mendaftar</p></div>";
          $this->session->set_flashdata($response);
          header('Location:'.base_url().'login');
        }
      }
    }

    public function checkNama(){
      $nomorinduk=$this->input->post('nomorinduk');
      $link= $this->link->getDataByNomorInduk($nomorinduk);
      if($link!=false){
        $user= $this->user->getUserByNomorInduk($nomorinduk);
        if($user!=false){
          $response['status']=false;
          $response['message']="Pengguna sudah pernah mendaftar";
        }else{
          $response['status']=true;
          $response['nama']=$link['nama'];
        }
      }else{
        $response['status']=false;
        $response['message']="Nomor Induk anda tidak tercatat pada database kami";
      }
      echo json_encode($response);
    }

    public function login(){
      // if(isset($this->input->post('username')&&!isset($this->input->post('password')))){
        $this->form_validation->set_rules('username','username','required|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
        $this->form_validation->set_rules('password','password','required|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));

        if($this->form_validation->run()==FALSE){
          $validation_errors=validation_errors();
          $this->session->set_flashdata(array('validation_errors'=>$validation_errors));
          header('Location:'.base_url().'login');
        }else{
          $username = $this->input->post('username',TRUE);
          $password = $this->input->post('password',TRUE);
          $password = md5('pass'.$password.'word');
          $login=$this->user->getUserByUsernamePassword($username,$password);
          // die($password);
          if($login!=false){
            $session['masuk_bukulink']=true;
            $session['noinduk']=$login['noinduk'];
            $session['username']=$login['username'];
            $session['status']=$login['status'];
            $session['nama']=$login['nama'];
            $this->session->set_userdata($session);
            header('Location:'.base_url().'home');
          }else{
            $this->session->set_flashdata(array('login_message'=>'Maaf username atau password yang anda masukkan salah'));
            header('Location:'.base_url().'login');
          }
        }
      }
    // }

    public function logout(){
      $this->session->sess_destroy();
      header('Location:'.base_url().'login');
    }
  }
?>
