<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MengelolaAkun extends CI_Controller{
  public function __construct(){
    parent::__construct();
    if(!$this->session->userdata('masuk_bukulink')){
      header('Location:'.base_url().'login');
    }
    $this->load->model('link');
    $this->load->model('user');
  }

  public function viewEditProfile($noinduk){
    if($noinduk==$this->session->userdata('noinduk')){
      $hasil=$this->link->getDataByNomorInduk($noinduk);
      $data['profile']=$hasil;
      $data['content']='editprofile';
      $this->load->view('template/main_template',$data);
    }else{
      header('location:'.base_url().'editprofile/'.$this->session->userdata('noinduk'));
    }
  }

  public function processEditProfile(){
    $this->form_validation->set_error_delimiters('<p class="text text-danger">','</p>');
    $this->form_validation->set_rules('editNama','Nama Lengkap','required|regex_match[/[A-Za-z\s\.\,]$/]|min_length[5]|max_length[30]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
    $this->form_validation->set_rules('editInstansi','Instansi','required|regex_match[/[A-Za-z\s\.]$/]|min_length[5]|max_length[50]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
    $this->form_validation->set_rules('editEmail','Email','required|valid_email|min_length[5]|max_length[30]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
    $this->form_validation->set_rules('editWebsite','Website','regex_match[/[0-9A-Za-z\s\.]$/]|min_length[8]|max_length[90]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
    $this->form_validation->set_rules('editKeterangan','Keterangan','regex_match[/[0-9A-Za-z\s]$/]|min_length[8]|max_length[100]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
    $this->form_validation->set_rules('editAlamat','Alamat','required|min_length[5]|max_length[60]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
    // $this->form_validation->set_rules('jenisLink','','required|numeric|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
    if($this->form_validation->run()==FALSE){
      $this->session->set_flashdata(array('validation_errors'=>validation_errors()));
      header('location:'.base_url().'editprofile/'.$this->session->userdata('noinduk'));
    }else{
      $noinduk=$this->session->userdata('noinduk');
      $nama=$this->input->post('editNama',TRUE);
      $instansi=$this->input->post('editInstansi',TRUE);
      $email=$this->input->post('editEmail',TRUE);
      $website=$this->input->post('editWebsite',TRUE);
      $keterangan=$this->input->post('editKeterangan',TRUE);
      $alamat=$this->input->post('editAlamat',TRUE);
      $link=$this->input->post('jenisLink',TRUE);
      $update=$this->link->update($noinduk,$nama,$instansi,$email,$website,$keterangan,$alamat,$link);
      if($update){
        $message['message']="<div class='alert alert-success'><h3><strong>Update Berhasil</strong></h3></div>";
      }else{
        $message['message']="<div class='alert alert-danger'><h3><strong>Update Gagal</strong></h3></div>";
      }
      $this->session->set_flashdata($message);
      header('Location:'.base_url().'editprofile/'.$this->session->userdata('noinduk'));
    }
  }

  public function editpassword(){
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger"></h5>','</h5></div>');
    $this->form_validation->set_rules('passwordLama','Password Lama','required|min_length[3]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
    $this->form_validation->set_rules('passwordLama','Password Lama','required|min_length[3]|trim|xss_clean|strip_tags',array('xss_clean'=>'Error Message : Your XSS is not clean'));
    if($this->form_validation->run()==FALSE){
      $response['validation_errors']=validation_errors();
      $this->session->set_flashdata($response);
      echo "<script> history.back()</script>";
    }else{
      $newpassword = $this->input->post('passwordBaru',TRUE);
      $newpassword = md5('pass'.$newpassword.'word');
      $oldpassword = $this->input->post('passwordLama',TRUE);
      $oldpassword = md5('pass'.$oldpassword.'word');
      $username = $this->session->userdata('username');
      $cek=$this->user->getUserByUsernamePassword($username,$oldpassword);
      if($cek!=false){
        $update=$this->user->updatePassword($username,$newpassword);
        if($update){
          $response['message_main_template']="<div class='alert alert-success'><h4>Ubah Password Berhasil</h4></div>";
        }else{
          $response['message_main_template']="<div class='alert alert-danger'><h4>Ubah Password Gagal</h4></div>";
        }
      }else{
        $response['message_main_template']="<div class='alert alert-danger'><h4>Password Lama Tidak Sesuai</h4></div>";
      }
      $this->session->set_flashdata($response);
      header('location:'.$_SERVER["HTTP_REFERER"]);
    }
  }

  public function terimaPengguna(){
    if($this->session->userdata('status')!=9){
      echo "<script> history.back()</script>";
    }else{
      $noinduk=$this->input->post('noinduk');
      // $noinduk=24010314130100;
      $update=$this->user->updateStatus($noinduk,1);
      // die(print_r($update));
      if($update){
        $response['status']=true;
      }else{
        $response['status']=false;
      }
      echo json_encode($response);
    }
  }

  public function batalkanPengguna(){
    if($this->session->userdata('status')!=9){
      echo "<script> history.back()</script>";
    }else{
      $noinduk=$this->input->post('noinduk');
      // $noinduk=24010314130100;
      $update=$this->user->updateStatus($noinduk,0);
      // die(print_r($update));
      if($update){
        $response['status']=true;
      }else{
        $response['status']=false;
      }
      echo json_encode($response);
    }

  }

  public function jadiAdmin(){
    if($this->session->userdata('status')!=9){
      echo "<script> history.back()</script>";
    }else{
      $noinduk=$this->input->post('noinduk');
      $update=$this->user->updateStatus($noinduk,9);
      if($update!=false){
        $response['status']=true;
      }else{
        $response['status']=false;
      }
      echo json_encode($response);
    }
  }

  public function hapusPengguna(){
    if($this->session->userdata('status')!=9){
      echo "<script> history.back()</script>";
    }
    $noinduk=$this->input->post('noinduk');
    $update= $this->user->delete($noinduk);
    if($update!=false){
      $response['status']=true;
    }else{
      $response['status']=false;
    }
    echo json_encode($response);
  }
}
 ?>
