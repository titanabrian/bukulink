<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('masuk_bukulink')){
			header('Location:'.base_url().'login');
		}
		$this->load->model('link');
		$this->load->model('user');
	}

	public function dashboard()
	{
		$result=$this->link->getDataByNomorInduk($this->session->userdata('noinduk'));
		$count_link=$this->link->countData();
		$count_user=$this->user->countData();
		if($result!=false){
			$jenislink=$this->link->getJenisLink($result['jenis_link']);
			if($jenislink!=false){
				$data['status']=$jenislink['nama'];
			}else{
				$data['status']='UNKNOWN';
			}
			$data['profile']=$result;
		}
		$data['count_link']=$count_link;
		$data['count_user']=$count_user;
		$data['content']='dashboard';
		$this->load->view('template/main_template',$data);
	}

	public function test(){
		$data['content']='test';
		$this->load->view('template/main_template',$data);
	}
}
