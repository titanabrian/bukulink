<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  class MenampilkanData extends CI_Controller{
    public function __construct(){
      parent::__construct();
      if(!$this->session->userdata('masuk_bukulink') || $this->session->userdata('status')==0){
  			header('Location:'.base_url().'login');
  		}

      $this->load->model('link');
      $this->load->model('user');
    }

    public function DataLengkap(){
      $data['datalengkap']=$this->link->getDataLengkap();
      $data['content']='data_lengkap';
      $this->load->view('template/main_template',$data);
    }

    public function dataDosen(){
      $data['datadosen']=$this->link->getDataDosen();
      $data['content']='data_dosen';
      $this->load->view('template/main_template',$data);
    }

    public function dataAlumni(){
      $data['dataalumni']=$this->link->getDataAlumni();
      $data['content']='data_alumni';
      $this->load->view('template/main_template',$data);
    }

    public function dataMahasiswa(){
      $data['datamahasiswa']=$this->link->getDataMahasiswa();
      $data['content']='data_mahasiswa';
      $this->load->view('template/main_template',$data);
    }

    public function dataLainnya(){
      $data['datalainnya']=$this->link->getDataLainnya();
      $data['content']='data_lainnya';
      $this->load->view('template/main_template',$data);
    }

    public function dataPengguna(){
      if($this->session->userdata('status')==9){
        $data['user']=$this->user->getAllUser();
        $data['content']='admin/kelola_pengguna';
        $this->load->view('template/main_template',$data);
      }else{
        echo "<script> history.back()</script>";
      }
    }

    public function fetchData(){
      if($this->session->userdata('status')!=0){
        $noinduk=$this->input->post('noinduk',TRUE);
        $result=$this->link->getDataByNomorInduk($noinduk);
        $status=$this->link->getJenisLink($result['jenis_link']);
        $response['data']=$result;
        $response['status']=$status;
        echo json_encode($response);
      }
    }
  }
?>
