<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MengelolaData extends CI_Controller{
  public function __construct(){
    parent::__construct();
    if(!$this->session->userdata('masuk_bukulink')){
      header('Location:'.base_url().'login');
    }

    $this->load->model('user');
    $this->load->model('link');
    $this->load->helper('file');
  }

  public function viewImport(){
    if($this->session->userdata('status')==9){
      $this->load->library('upload');
      if(isset($_POST['submit'])){
        $this->form_validation->set_rules('file','','callback_file_check');
        if($this->form_validation->run()==TRUE){
          $config['upload_path']='./uploads/files/';
          $config['allowed_types']='xls|xlsx';
          $config['max_size']=1024;
          $this->upload->initialize($config);
          if($this->upload->do_upload('file')){
            $uploadData=$this->upload->data();
            $this->insertData($uploadData);
            $data['success_msg']='File '.$uploadedFile.' berhasil di-Import';
          }else{
            $data['error_msg']=$this->upload->display_errors();
          }
        }
      }
      $data['content']="admin/import_data";
      $this->load->view('template/main_template',$data);
    }else{
      header('Location:'.base_url().'home');
    }
  }

  private function insertData($file){
    $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
    $fileName=$file['file_name'];
    $inputFileName='./uploads/files/'.$fileName;
    try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
            // die(print_r($objPHPExcel));
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestDataRow();
        $highestColumn = $sheet->getHighestDataColumn();
        // die($highestColumn);
        for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);

            //Sesuaikan sama nama kolom tabel di database

             $data = array(
                "nama"=> (string)$rowData[0][0],
                "no_induk"=> (string)$rowData[0][1],
                "instansi"=> (string)$rowData[0][2],
                "alamat"=> (string)$rowData[0][3],
                "telepon"=>(string)$rowData[0][4],
                "email"=>(string)$rowData[0][5],
                "website"=>(string)$rowData[0][6],
                "keterangan"=>(string)$rowData[0][7],
                "foto"=>(string)$rowData[0][8],
                "jenis_link"=>$rowData[0][9]
                //die(print_r($data));
            );
            if($rowData[0][0]==null){
              $data['nama']=="Unknown";
            }
            if($rowData[0][1]==null){
              $data['no_induk']=="Unknown";
            }
            if($rowData[0][9]==null){
              break;
            }
             $this->link->insert($data);
        }
        unlink('./uploads/files/'.$fileName);
  }

  public function file_check($string){
    $allowed=array('application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    $mime=get_mime_by_extension($_FILES['file']['name']);
    if(isset($_FILES['file']['name'])&& $_FILES['file']['name']!=''){
      if(in_array($mime,$allowed)){
        return true;
      }else{
        return false;
      }
    }else{
      $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
      return false;
    }
  }
}

 ?>
