<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  class Link extends CI_Model{
    public function __construct(){
      parent:: __construct();
    }

    public function countData(){
      $totaldata = "SELECT COUNT(*) AS jumlah FROM daftar_link";
      $totaldata = $this->db->query($totaldata)->row_array();
      $totaldosen = "SELECT COUNT(*) AS jumlah FROM daftar_link INNER JOIN jenis_link ON daftar_link.jenis_link=jenis_link.id WHERE jenis_link.nama='Dosen'";
      $totaldosen = $this->db->query($totaldosen)->row_array();
      $totalmahasiswa = "SELECT COUNT(*) AS jumlah FROM daftar_link INNER JOIN jenis_link ON daftar_link.jenis_link=jenis_link.id WHERE jenis_link.nama='Mahasiswa'";
      $totalmahasiswa = $this->db->query($totalmahasiswa)->row_array();
      $totalalumni = "SELECT COUNT(*) AS jumlah FROM daftar_link INNER JOIN jenis_link ON daftar_link.jenis_link=jenis_link.id WHERE jenis_link.nama='Alumni'";
      $totalalumni = $this->db->query($totalalumni)->row_array();
      $totallainnya = "SELECT COUNT(*) AS jumlah FROM daftar_link INNER JOIN jenis_link ON daftar_link.jenis_link=jenis_link.id WHERE jenis_link.nama='Lain-lain' OR jenis_link.nama='Birokrasi' OR jenis_link.nama='Toko Komputer' OR jenis_link.nama='Universitas Lain' OR jenis_link.nama='Kepanitiaan' ";
      $totallainnya = $this->db->query($totallainnya)->row_array();

      $data['totaldata']=$totaldata['jumlah'];
      $data['totaldosen']=$totaldosen['jumlah'];
      $data['totalmahasiswa']=$totalmahasiswa['jumlah'];
      $data['totalalumni']=$totalalumni['jumlah'];
      $data['totallainnya']=$totallainnya['jumlah'];
      return $data;
    }

    public function getDataLengkap(){
      $query="SELECT * FROM daftar_link";
      $result = $this->db->query($query);
      if($result->num_rows()>0){
        return $result->result();
      }else{
        return false;
      }
    }

    public function getDataDosen(){
      $query="SELECT *,daftar_link.nama AS nama_link FROM daftar_link INNER JOIN jenis_link ON daftar_link.jenis_link=jenis_link.id WHERE jenis_link.nama='Dosen'";
      $result = $this->db->query($query);
      if($result->num_rows()>0){
        return $result->result();
      }else{
        return false;
      }
    }

    public function getDataAlumni(){
      $query="SELECT *, daftar_link.nama AS nama_link FROM daftar_link INNER JOIN jenis_link ON daftar_link.jenis_link=jenis_link.id WHERE jenis_link.nama='Alumni'";
      $result = $this->db->query($query);
      if($result->num_rows()>0){
        return $result->result();
      }else{
        return false;
      }
    }

    public function getDataMahasiswa(){
      $query="SELECT *,daftar_link.nama AS nama_link FROM daftar_link INNER JOIN jenis_link ON daftar_link.jenis_link=jenis_link.id WHERE jenis_link.nama='Mahasiswa'";
      $result = $this->db->query($query);
      if($result->num_rows()>0){
        return $result->result();
      }else{
        return false;
      }
    }

    public function getDataLainnya(){
      $query="SELECT *, daftar_link.nama AS nama_link FROM daftar_link INNER JOIN jenis_link ON daftar_link.jenis_link=jenis_link.id WHERE NOT (jenis_link.nama='Dosen' OR jenis_link.nama ='Alumni' OR jenis_link.nama!='Mahasiswa')";
      $result = $this->db->query($query);
      if($result->num_rows()>0){
        return $result->result();
      }else{
        return false;
      }
    }

    public function getDataByNomorInduk($nomorinduk){
      $query=" SELECT * FROM daftar_link WHERE no_induk=?";
      $result =$this->db->query($query,$nomorinduk);
      if($result->num_rows()>0){
        return $result->row_array();
      }else{
        return false;
      }
    }

    public function getJenisLink($id){
      $query = "SELECT * FROM jenis_link WHERE id='$id'";
      $result = $this->db->query($query);
      if($result->num_rows()>0){
        return $result->row_array();
      }else{
        return false;
      }
    }

    public function update($noinduk,$nama,$instansi,$email,$website,$keterangan,$alamat,$jenis_link){
      $query="UPDATE daftar_link SET nama=?, instansi=?, email=?, website=?, keterangan=?, alamat=? WHERE no_induk=?";
      return $this->db->query($query,array($nama,$instansi,$email,$website,$keterangan,$alamat,$noinduk));
    }

    public function insert($data){
      if($this->getDataByNomorInduk($data['no_induk'])!=false){
        return $this->update($data['no_induk'],$data['nama'],$data['instansi'],$data['email'],$data['website'],$data['keterangan'],$data['alamat'],$data['jenis_link']);
      }else{
        return $this->db->insert('daftar_link',$data);
      }
    }
  }

 ?>
