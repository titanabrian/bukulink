<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  class User extends CI_Model{
    public function countData(){
      $totaluser="SELECT COUNT(*) AS jumlah FROM up_link";
      $totaluser=$this->db->query($totaluser)->row_array();
      $totalconfirmed="SELECT COUNT(*) AS jumlah FROM up_link WHERE status=1 OR status=9";
      $totalconfirmed=$this->db->query($totalconfirmed)->row_array();
      $totalunconfirmed="SELECT COUNT(*) AS jumlah FROM up_link WHERE status=0";
      $totalunconfirmed=$this->db->query($totalunconfirmed)->row_array();

      $data['totaluser']=$totaluser['jumlah'];
      $data['totalconfirmed']=$totalconfirmed['jumlah'];
      $data['totalunconfirmed']=$totalunconfirmed['jumlah'];
      return $data;
    }

    public function getAllUser(){
      $query="SELECT * FROM up_link";
      $result = $this->db->query($query);
      if($result->num_rows()>0){
        return $result->result();
      }else{
        return false;
      }
    }

    public function getUserByUsernamePassword($username,$password){
      $query="SELECT * FROM up_link INNER JOIN daftar_link ON up_link.noinduk = daftar_link.no_induk WHERE username=? AND password=?";
      $result = $this->db->query($query,array($username,$password));
      if($result->num_rows()==1){
        return $result->row_array();
      }else{
        return false;
      }
    }

    public function getUserByNomorInduk($nomorinduk){
      $query=" SELECT * FROM up_link WHERE noinduk=?";
      $result =$this->db->query($query,$nomorinduk);
      if($result->num_rows()>0){
        return $result->row_array();
      }else{
        return false;
      }
    }

    public function insert($username,$password,$email,$noinduk,$status){
      $query="INSERT INTO up_link(username,password,email,noinduk,status) VALUES (?,?,?,?,?)";
      return $this->db->query($query,array($username,$password,$email,$noinduk,$status));
    }

    public function updatePassword($username,$password){
      $query="UPDATE up_link SET password = ? WHERE username=?";
      return $this->db->query($query,array($password,$username));
    }

    public function updateStatus($noinduk,$status){
      $query="UPDATE up_link SET status = ? WHERE noinduk=?";
      return $this->db->query($query,array($status,$noinduk));
    }

    public function delete($noinduk){
      $query="DELETE FROM up_link WHERE noinduk=?";
      return $this->db->query($query,array($noinduk));
    }
  }
 ?>
