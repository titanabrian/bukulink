<!-- Counts Section -->
<section class="dashboard-counts section-padding">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-2 col-md-4 col-6">
        <div class="wrapper count-title d-flex">
          <div class="icon"><i class="icon-user"></i></div>
          <div class="name"><strong class="text-uppercase">Total Data</strong><span>Internal & Eksternal</span>
            <div class="count-number" id="totaldata"><?=$count_link['totaldata']?></div>
          </div>
        </div>
      </div>
      <div class="col-xl-2 col-md-4 col-6">
        <div class="wrapper count-title d-flex">
          <div class="icon"><i class="icon-user"></i></div>
          <div class="name"><strong class="text-uppercase">Dosen</strong>
            <div class="count-number" id="totaldosen"><?=$count_link['totaldosen']?></div>
          </div>
        </div>
      </div>
      <div class="col-xl-2 col-md-4 col-6">
        <div class="wrapper count-title d-flex">
          <div class="icon"><i class="icon-user"></i></div>
          <div class="name"><strong class="text-uppercase">Alumni</strong>
            <div class="count-number" id="totalalumni"><?=$count_link['totalalumni']?></div>
          </div>
        </div>
      </div>
      <div class="col-xl-2 col-md-4 col-6">
        <div class="wrapper count-title d-flex">
          <div class="icon"><i class="icon-user"></i></div>
          <div class="name"><strong class="text-uppercase">Mahasiswa</strong>
            <div class="count-number" id="totalmahasiswa"><?=$count_link['totalmahasiswa']?></div>
          </div>
        </div>
      </div>
      <div class="col-xl-2 col-md-4 col-6">
        <div class="wrapper count-title d-flex">
          <div class="icon"><i class="icon-user"></i></div>
          <div class="name"><strong class="text-uppercase">Lainnya</strong>
            <div class="count-number" id="totallainnya"><?=$count_link['totallainnya']?></div>
          </div>
        </div>
      </div>
      <div class="col-xl-2 col-md-4 col-6">
        <div class="wrapper count-title d-flex">
          <div class="icon"><i class="icon-user"></i></div>
          <div class="name"><strong class="text-uppercase">Pengguna</strong><span><?=$count_user['totalunconfirmed']?> Unconfirmed</span>
            <div class="count-number"><?=$count_user['totalconfirmed']?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
<hr>
</section>
<section class="dashboard-header section-padding">
  <div class="row align-items-md-stretch">
    <div class="col-md-12">
      <input hidden value="<?=$this->session->userdata('status')?>">
      <h1 class="text" style="margin-left:10px">Selamat datang <strong id="strong-nama"><?=$this->session->userdata('nama')?>,</strong> kamu <i id="pesan-konfirmasi" class="text text-success">Confirmed</i></h1>
    </div>
  </div>
</section>
<!-- Header Section-->
<section class="dashboard-header section-padding">
  <div class="container-fluid">
    <div class="row align-items-md-stretch">
      <!-- Pie Chart-->
      <div class="col-lg-5 col-md-8">
        <div class="wrapper project-progress">
          <h2 class="display h4">Persebaran Data</h2>
          <p> Grafik persebaran data link</p>
          <div class="pie-chart">
            <canvas id="pieChart"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg-5 col-md-7">
        <h3 class="text-primary">Profile</h3><hr>
        <div class="form-group row">
          <label class="col-sm-2 form-control-label">Nama Lengkap</label>
          <div class="col-sm-10">
            <input type="text" disabled id="nama" name="nama" value="<?=$profile['nama']?>" class="form-control">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 form-control-label">Email</label>
          <div class="col-sm-10">
            <input type="text" disabled id="email" name="email" value="<?=$profile['email']?>" class="form-control">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 form-control-label">Alamat</label>
          <div class="col-sm-10">
            <input type="text" disabled id="alamat" name="alamat" value="<?=$profile['alamat']?>" class="form-control">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 form-control-label">Status</label>
          <div class="col-sm-10">
            <input type="text" disabled id="status" name="status" value="<?=$status?>" class="form-control">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-10 centered">
            <a style="align:right" href="<?=base_url()?>editprofile/<?=$profile['no_induk']?>"><button class="btn btn-default"><i class="fa fa-edit"></i>Edit</button></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    var x = <?=$this->session->userdata('status')?>;
    if(x==1){
      $('#pesan-konfirmasi').attr('class','text text-success');
      $('#pesan-konfirmasi').html('Confirmed, Anda dapat melihat data dan merubah profile anda');
    }else if(x==0){
      $('#pesan-konfirmasi').attr('class','text text-danger');
      $('#pesan-konfirmasi').html('Unconfirmed, belum dapat melihat data dan merubah profile anda');
    }else if(x==9){
      $('#pesan-konfirmasi').attr('class','text text-success');
      $('#pesan-konfirmasi').html('Admin');
    }
  });
</script>
