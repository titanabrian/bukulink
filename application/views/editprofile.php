<style media="screen">
  .paginate_button{
    margin-left:10px;
  }
  .dataTable_info{
    margin-right: 20px;
  }
</style>
<section class="charts">
  <div class="container-fluid">
    <header>
      <h1 class="h1"><strong><span class="text-brand">Edit <span class="text-primary">Profile</span></strong></h1>
    </header>
    <div class="row">
      <div class="col-md-12">
        <?=$this->session->flashdata('message')?>
        <div class="card">
          <div class="card-header d-flex align-items-center">
            <h1 class="brand-small"><?=$this->session->userdata('nama')?></h1>
            <hr style="margin-top:5px"/>
          </div>
          <div class="card-body">
            <div class="">
              <?=form_open(base_url().'editprofile/process',array('class'=>'form'));?>
              <?=$this->session->flashdata('validation_errors')?>
              <div class="form-group row">
                <label class="col-lg-2 form-control-label">Nomor Induk</label>
                <div class="col-lg-10">
                  <input type="text" name="editNomorinduk" disabled="" value="<?=$profile['no_induk']?>" class="form-control">
                </div>
              </div>
              <div class="line"> </div>
              <div class="form-group row">
                <label class="col-sm-2 form-control-label">Nama Lengkap</label>
                <div class="col-sm-10">
                  <input type="text" id="edit-nama" name="editNama" value="<?=$profile['nama']?>" class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-2 form-control-label">Email</label>
                <div class="col-sm-10">
                  <input type="text" id="edit-email" name="editEmail" value="<?=$profile['email']?>" class="form-control">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 form-control-label">Alamat</label>
                <div class="col-sm-10">
                  <input type="text" id="edit-alamat" name="editAlamat" value="<?=$profile['alamat']?>" class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-2 form-control-label">Telephone</label>
                <div class="col-sm-10">
                  <input type="text" id="edit-telephone" name="editTelephone" value="<?=$profile['telepon']?>" class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-2 form-control-label">Instansi</label>
                <div class="col-sm-10">
                  <input type="text" id="edit-instansi" name="editInstansi" value="<?=$profile['instansi']?>" class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-2 form-control-label">Website</label>
                <div class="col-sm-10">
                  <input type="text" id="edit-website" name="editWebsite" value="<?=$profile['website']?>" class="form-control">
                </div>
              </div>

              <div class="line"></div>
              <div class="form-group row">
                <label class="col-sm-2 form-control-label">Keterangan</label>
                <div class="col-sm-10">
                  <input type="text" id="edit-keterangan" name="editKeterangan" value="<?=$profile['keterangan']?>" class="form-control">
                </div>
              </div>
              <input type="hidden" disabled name="jenisLink" value="<?=$profile['jenis_link']?>">
              <div class="line"></div>
              <div class="form-group row">
                <div class="col-sm-4 offset-sm-2">
                  <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                </div>
              </div>
              <?=form_close();?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    $('#table-data').DataTable();
  });
</script>
