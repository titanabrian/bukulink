<style media="screen">
  .paginate_button{
    margin-left:10px;
  }
  .dataTable_info{
    margin-right: 20px;
  }
</style>
<section class="charts">
  <div class="container-fluid">
    <header>
      <h1 class="h1"><strong><span class="text-brand">Import <span class="text-primary">Data</span></strong></h1>
    </header>
    <div class="row">
      <div class="col-md-6">
        <?=$this->session->flashdata('message')?>
        <div class="card">
          <div class="card-header d-flex align-items-center">
            <h1 class="brand-small">Upload FIle</h1>
            <hr style="margin-top:5px"/>
          </div>
          <div class="card-body">
            <div class="">
              <?=form_open('',array('class'=>'form','enctype'=>'multipart/form-data'));?>
              <?=$this->session->flashdata('validation_errors')?>
              <div class="form-group row">
                <h3 class=""></h3>
                <div class="col-md-12">
                  <input type="file" name="file"class="form-control">
                </div>
              </div>
              <?php if(!empty($success_msg)){
                echo "<div class='alert alert-success'><p>".$success_msg."</p></div>";
              }else if(!empty($error_msg)){
                echo "<div class='alert alert-danger'><p>".$serror_msg."</p></div>";
              } ?>
              <?=form_error('file','<div class="alert alert-danger"><p>','</p></div>')?>
              <div class="col-md-12">
                <p>Unggah file dengan tipe *.xls</p>
              </div>
              <div class="form-group row">
                <h3 class=""></h3>
                <div class="col-md-12" style="content-align:center">
                  <button type="submit" name="submit"class="col-md-offset-3 col-md-6 form-control btn-success">Simpan</button>
                </div>
              </div>
              <?=form_close();?>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    $('#table-data').DataTable();
  });
</script>
