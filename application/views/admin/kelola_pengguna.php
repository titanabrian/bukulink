<style media="screen">
  .paginate_button{
    margin-left:20px;
  }
  .dataTable_info{
    margin-right: 20px;
  }
</style>
<script type="text/javascript" src="<?=base_url()?>bootstrap/js/bukulink.js"></script>
<section class="charts">
  <div class="container-fluid">
    <header>
      <h1 class="h1"><strong>Database <span class="text-primary">Ilmu Komputer/</span> <span class="brand-small text-center">Informatika</span></strong></h1>
    </header>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header d-flex align-items-center">
            <h1 class="brand-small">Data <span class="text-primary">Pengguna</span></h1>
            <hr style="margin-top:5px"/>
          </div>
          <div class="card-body">
            <table id="table-data" class="table table-striped table-hover">
              <thead class="sorting">
                <tr>
                  <th>No</th>
                  <th>Admin</th>
                  <th>Kelola</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Nomor Induk</th>
                </tr>
              </thead>
              <tbody>
                <?php if($user!=false){
                  $i=1;
                  foreach($user as $row){
                    echo "<tr>";
                    echo '<td scope="row">'.$i.'</td>';
                    if($row->noinduk!=$this->session->userdata('noinduk')){
                      if($row->status!=9){
                        echo '<td><button onclick="tambahAdmin('.$row->noinduk.')" class="btn btn-default" data-toggle="tooltip" title="jadikan admin" style="background-color:#337ab7;border-color:#2e6da4"><i class="fa fa-check"></button></td>';
                      }else{
                        echo '<td><button onclick="terima('.$row->noinduk.')" class="btn btn-default" data-toggle="tooltip" title="batalkan status admin"><i class="fa fa-close"></button></td>';
                      }
                      if($row->status==0){
                        echo '<td><button class="btn btn-info" data-toggle="tooltip" title="detail pengguna" onclick="detail(\''.$row->noinduk.'\')">Detail</button><button onclick="terima('.$row->noinduk.')" class="btn btn-default" data-toggle="tooltip" title="konfirmasi pengguna"><i class="fa fa-check"></i></button><button onclick="hapus('.$row->noinduk.')" class="btn btn-danger" data-toggle="tooltip" title="hapus pengguna ini"><i class="fa fa-trash"></i></button></td>';
                      }else{
                        echo '<td><button class="btn btn-info" data-toggle="tooltip" title="detail pengguna" onclick="detail(\''.$row->noinduk.'\')">Detail</button><button onclick="batal('.$row->noinduk.')" class="btn btn-primary" data-toggle="tooltip" title="batalkan status konfirmasi pengguna"><i class="fa fa-close"></i></button><button onclick="hapus('.$row->noinduk.')" class="btn btn-danger" data-toggle="tooltip" title="hapus pengguna ini"><i class="fa fa-trash"></i></button></td>';
                      }
                    }else{
                      echo '<td><button class="btn btn-default"><i class="fa fa-ban "></i></button></td>';
                      echo '<td><button class="btn btn-info" data-toggle="tooltip" title="detail pengguna" onclick="detail(\''.$row->noinduk.'\')">Detail</button><button class="btn btn-primary" data-toggle="tooltip" title="batalkan pengguna"><i class="fa fa-ban"></i></button><button class="btn btn-danger" data-toggle="tooltip" title="hapus pengguna"><i class="fa fa-ban"></i></button></td>';
                    }
                    echo '<td>'.$row->username.'</td>';
                    echo '<td>'.$row->email.'</td>';
                    echo '<td>'.$row->noinduk.'</td>';
                    echo "</tr>";
                    $i++;
                  }
                }?>
              </tbody>
              <tfoot>
              </tfoot>
            </table>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    $('#table-data').DataTable();
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
