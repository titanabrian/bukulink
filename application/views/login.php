<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bukulink</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/css/fontastic.css">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/css/grasp_mobile_progress_circle-1.0.0.min.css">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/css/custom.css">
    <link rel="icon" href="http://hm.if.undip.ac.id/assets/images/logo.png">
  </head>
  <body>
    <div class="page login-page">
      <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner">
            <div class="logo text-uppercase"><span>BUKU</span><strong class="text-primary">LINK</strong></div>
            <p>Bukulink menampung data - data dari anggota Himpunan Mahasiswa Informatika Universitas Dipongoro dan juga data link eksternal seperti perusahaan maupun unit usaha yang ada di lingkungan masyarakat</p>
            <br>
            <?=$this->session->flashdata('message_register')?>
            <?=$this->session->flashdata('validation_errors');?>
            <?=form_open(base_url().'login/login','id="login-form"')?>
              <div class="form-group">
                <label for="login-username" class="label-custom">Username</label>
                <input id="login-username" type="text" name="username" required="">
              </div>
              <div class="form-group">
                <label for="login-password" class="label-custom">Password</label>
                <input id="login-password" type="password" name="password" required="">
                <p class="text text-danger"><?=$this->session->flashdata('login_message')?></p>
              </div><button id="login" type="submit" value="submit" class="btn btn-primary">Login</button>
            <?=form_close()?>
          </form><small>Belum memiliki akun ? </small><a href="<?=base_url()?>register" class="signup">Buat Akun</a>
          </div>
          <div class="copyrights text-center">
        </div>
      </div>
    </div>
    <!-- Javascript files-->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"> </script>
    <script src="<?=base_url()?>bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>bootstrap/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="<?=base_url()?>bootstrap/js/grasp_mobile_progress_circle-1.0.0.min.js"></script>
    <script src="<?=base_url()?>bootstrap/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?=base_url()?>bootstrap/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?=base_url()?>bootstrap/js/front.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
    <!---->
    <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
  </body>
</html>
