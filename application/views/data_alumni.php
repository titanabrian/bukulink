<style media="screen">
  .paginate_button{
    margin-left:10px;
  }
  .dataTable_info{
    margin-right: 20px;
  }
</style>
<section class="charts">
  <div class="container-fluid">
    <header>
      <h1 class="h1"><strong>Database <span class="text-primary">Ilmu Komputer/</span> <span class="brand-small text-center">Informatika</span></strong></h1>
    </header>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header d-flex align-items-center">
            <h1 class="brand-small">Data <span class="text-primary">Alumni</span></h1>
            <hr style="margin-top:5px"/>
          </div>
          <div class="card-body">
            <table id="table-data" class="table table-striped table-hove">
              <thead class="sorting">
                <tr>
                  <th>No</th>
                  <th>#</th>
                  <th>No Induk</th>
                  <th>Nama Lengkap</th>
                  <th>Telepon</th>
                  <th>Email</th>
                </tr>
              </thead>
              <tbody>
                <?php if($dataalumni!=false){
                  $i=1;
                  foreach($dataalumni as $row){
                    echo "<tr>";
                    echo '<th scope="row">'.$i.'</th>';
                    echo '<td><button onclick="detail(\''.$row->no_induk.'\')" class="btn btn-info">Detail</button></td>';
                    echo '<td>'.$row->no_induk.'</td>';
                    echo '<td>'.$row->nama_link.'</td>';
                    echo '<td>'.$row->telepon.'</td>';
                    echo '<td>'.$row->email.'</td>';
                    echo "</tr>";
                    $i++;
                  }
                }?>
              </tbody>
              <tfoot>

              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    $('#table-data').DataTable();
  });
</script>
