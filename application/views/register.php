<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bukulink</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/css/fontastic.css">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/css/grasp_mobile_progress_circle-1.0.0.min.css">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/css/custom.css">
    <link rel="icon" href="http://hm.if.undip.ac.id/assets/images/logo.png">
  </head>
  <body>
    <div class="page login-page">
      <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner">
            <div class="logo text-uppercase"><span>BUKU</span><strong class="text-primary">Link</strong></div>
            <p>Silahkan mengisi formulir dibawah ini secara lengkap.<strong>Jika nomor induk tidak tersedia pada sistem ini, silahkan menghubungi KOMINFO HMIF</strong></p></br>
            <?=$this->session->flashdata('validation_errors')?>
            <?=form_open(base_url().'register/process',array('class'=>'register-form'))?>
            <!-- <form id="register-form"> -->
              <div class="form-group">
                <label for="register-username" class="label-custom">Username</label>
                <input id="register-username" type="text" name="registerUsername">
              </div>
              <div class="form-group">
                <label for="register-nomorinduk" class="label-custom">Nomor Induk</label>
                <input id="register-nomorinduk" type="text" name="registerNomorInduk">
              </div>
              <div class="form-group">
                <!-- <label for="register-nama" class="label-custom">Nama</label> -->
                <input id="register-nama" type="text" name="registerNama" disabled value="">
              </div>
              <div class="form-group">
                <label for="register-email" class="label-custom">Email </label>
                <input id="register-email" name="registerEmail">
              </div>
              <div class="form-group">
                <label for="register-passowrd" class="label-custom">Password</label>
                <input id="register-passowrd" type="password" name="registerPassword">
              </div>
              <div class="form-group">
                <p class="text text-danger" id="error_nomorinduk"></p>
                <p class="text text-danger"><?=$this->session->flashdata('message')?></p>
              </div>
              <input id="register" type="submit" value="Register" class="btn btn-primary">
            <?=form_close()?>
            <small>Already have an account? </small><a href="<?=base_url()?>login" class="signup">Login</a>
          </div>
          <div class="copyrights text-center">
          </div>
        </div>
      </div>
    </div>
    <!-- Javascript files-->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"> </script>
    <script src="<?=base_url()?>bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>bootstrap/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="<?=base_url()?>bootstrap/js/grasp_mobile_progress_circle-1.0.0.min.js"></script>
    <script src="<?=base_url()?>bootstrap/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?=base_url()?>bootstrap/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?=base_url()?>bootstrap/js/front.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
    <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');

      $(document).ready(function(){
        var nama = $('#register-nama');
        var nomorinduk = $('#register-nomorinduk');
        var errornomorinduk = $('#error_nomorinduk')
        nama.hide();
        errornomorinduk.hide();
        nomorinduk.change(function(){
          $.ajax({
            method: "post",
            url:"<?=base_url()?>login/checknama",
            data:{'nomorinduk':nomorinduk.val()},
            dataType:"json",
            success:function(data){
              if(data.status){
                errornomorinduk.html('');
                errornomorinduk.hide();
                nama.val(data.nama);
                nama.show();
              }else{
                errornomorinduk.html(data.message);
                errornomorinduk.show();
                nama.val('');
                nama.hide();
                nomorinduk.focus();
                return false;
              }
            }
          });
        });
      });
    </script>
  </body>
</html>
