<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bukulink</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/css/fontastic.css">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/css/grasp_mobile_progress_circle-1.0.0.min.css">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/css/custom.css">
    <link rel="stylesheet" href="<?=base_url()?>bootstrap/data-tables/datatables.min.css">
    <!-- Favicon-->
    <link rel="icon" href="http://hm.if.undip.ac.id/assets/images/logo.png">
  </head>
  <body>
    <script type="text/javascript" src="<?=base_url()?>bootstrap/js/sweetalert.min.js">

    </script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="<?=base_url()?>bootstrap/data-tables/datatables.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>bootstrap/js/bukulink.js"></script>
    <!-- SIDER -->
    <?php $this->load->view('template/sider')?>
    <!-- SIDER -->

    <div class="page home-page">
      <!-- navbar-->
      <header class="header">
        <nav class="navbar">
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="index.html" class="navbar-brand">
              </div>
              <ul class="nav-menu list-unstyled d-flex  align-items-md-center">
                <?php if($this->session->userdata('status')!=0){?>
                <li class="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i></a>
                  <ul aria-labelledby="notifications" class="dropdown-menu">
                    <li><a rel="nofollow" href="<?=base_url()?>editprofile/<?=$this->session->userdata('noinduk')?>" class="dropdown-item d-flex">
                        <div data-toggle="modal" data-targer="#editpassword" class="msg-profile"><i class="fa fa-user"></i>Edit Profile</div></a>
                    </li>
                    <li><a rel="nofollow" id="btn-editpassword" class="dropdown-item d-flex">
                        <div class="msg-profile" ><i class="fa fa-pencil"></i>Ubah Password</div></a>
                    </li>
                  </ul>
                </li>
              <?php }?>
                <li class="nav-item"><a href="<?=base_url()?>login/logout" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <section>
        <?=$this->session->flashdata('validation_errors')?>
        <?=$this->session->flashdata('message_main_template')?>
      </section>
      <?php if($this->session->userdata('status')!=0){?>
      <section>
        <div id="editpassword" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header" style="background:#28a745">
                <h4 style="color:white">Ubah Password</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!-- <h4 class="modal-title">Modal Header</h4> -->
              </div>
              <div class="modal-body">
                <?=form_open(base_url().'editpassword/process')?>
                  <div class="form-group row">
                    <label class="col-sm-4  form-control-label">Password Baru</label>
                    <div class="col-sm-8">
                      <input type="password" id="password-baru" name="passwordBaru"class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-4  form-control-label">Password Lama</label>
                    <div class="col-sm-8">
                      <input type="password" id="password-lama" name="passwordLama" class="form-control">
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit"class="btn btn-success" name="submit" id="btn-save">Simpan</button>
                <?=form_close()?>
              </div>
            </div>
          </div>
        </div>
      </section>
    <?php } ?>
      <!-- CONTENT -->
      <?php $this->load->view($content)?>
      <!-- CONTENT -->
      <footer class="main-footer">
        <!---MODAL-->
        <div id="detail-pengguna" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header" style="background:#28a745">
                <h4 style="color:white">Detail Pengguna</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!-- <h4 class="modal-title">Modal Header</h4> -->
              </div>
              <div class="modal-body">
                <table class="table table-striped table-hover">
                  <tbody>
                    <tr>
                      <td style="font-weight:bold">Nama</td>
                      <td id="detail-nama" ></td>
                    </tr>
                    <tr>
                      <td style="font-weight:bold">Nomor Induk</td>
                      <td id="detail-noinduk"></td>
                    </tr>
                    <tr>
                      <td style="font-weight:bold">Instansi</td>
                      <td id="detail-instansi"></td>
                    </tr>
                    <tr>
                      <td style="font-weight:bold">Alamat</td>
                      <td id="detail-alamat"></td>
                    </tr>
                    <tr>
                      <td style="font-weight:bold">Email</td>
                      <td id="detail-email"></td>
                    </tr>
                    <tr>
                      <td style="font-weight:bold">Telepon</td>
                      <td id="detail-telepon"></td>
                    </tr>
                    <tr>
                      <td style="font-weight:bold">Status</td>
                      <td id="detail-status"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </div>
        <!---MODAL-->
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>&copy; HMIF Undip 2017</p>
            </div>
            <div class="col-sm-6 text-right">


              <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
          </div>
        </div>
      </footer>
    </div>
    <!-- Javascript files-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"> </script>
    <script src="<?=base_url()?>bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>bootstrap/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="<?=base_url()?>bootstrap/js/grasp_mobile_progress_circle-1.0.0.min.js"></script>
    <script src="<?=base_url()?>bootstrap/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?=base_url()?>bootstrap/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script src="<?=base_url()?>bootstrap/js/charts-home.js"></script>
    <script src="<?=base_url()?>bootstrap/js/front.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
    <!---->
    <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');

      $(document).ready(function(){
        $('#btn-editpassword').click(function(){
          $('#editpassword').modal({show:true});
        });
      });
    </script>
  </body>
</html>
