<!-- Side Navbar -->
<style media="screen">
  .collapse-active{
    background-color: black;
  }
</style>
<nav class="side-navbar">
  <div class="side-navbar-wrapper">
    <div class="sidenav-header d-flex align-items-center justify-content-center">
      <div class="sidenav-header-inner text-center">
        <h2 class="h5 text-uppercase"><?=$this->session->userdata('nama')?></h2>
      </div>
      <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>B</strong><strong class="text-primary">L</strong></a></div>
    </div>
    <!-- <div class="main-menu">
      <ul id="side-main-menu" class="side-menu list-unstyled">
        <li class="active"><a href="index.html"> <i class="icon-home"></i><span>Home</span></a></li>
        <li> <a href="forms.html"><i class="icon-form"></i><span>Forms</span></a></li>
        <li> <a href="charts.html"><i class="icon-presentation"></i><span>Charts</span></a></li>
        <li> <a href="tables.html"> <i class="icon-grid"> </i><span>Tables  </span></a></li>
        <li> <a href="login.html"> <i class="icon-interface-windows"></i><span>Login page                        </span></a></li>
        <li> <a href="#"> <i class="icon-mail"></i><span>Demo</span>
            <div class="badge badge-warning">6 New</div></a></li>
      </ul>
    </div> -->
    <div class="admin-menu">
      <ul id="side-admin-menu" class="side-menu list-unstyled">
        <li> <a href="#pages-nav-home" id="home" data-toggle="collapse" aria-expanded="false"><i class="icon-home"></i><span>Home</span>
          <div class="arrow pull-right"><i class="fa fa-angle-down"></i></div></a>
          <ul id="pages-nav-home" class=" collapse list-unstyled active">
            <li> <a  href="<?=base_url()?>home/dashboard" id="sider-dashboard">Dashboard</a></li>
            <li> <a href="<?=base_url()?>editprofile/<?=$this->session->userdata('noinduk')?>" id="sider-editprofil">Edit Profile</a></li>
          </ul>
        </li>
        <?php if($this->session->userdata('status')!=0){ ?>
        <li> <a href="#pages-nav-dataanggota" id="data" data-toggle="collapse" aria-expanded="false"><i class="icon-interface-windows"></i><span>Data Anggota</span>
            <div class="arrow pull-right"><i class="fa fa-angle-down"></i></div></a>
          <ul id="pages-nav-dataanggota" class="collapse list-unstyled">
            <li> <a href="<?=base_url()?>data/datalengkap" id="data-lengkap">Data Lengkap</a></li>
            <li> <a href="<?=base_url()?>data/datadosen" id="data-dosen">Data Dosen</a></li>
            <li> <a href="<?=base_url()?>data/dataalumni" id="data-alumni">Data Alumni</a></li>
            <li> <a href="<?=base_url()?>data/datamahasiswa" id="data-mahasiswa">Data Mahasiswa</a></li>
            <li> <a href="<?=base_url()?>data/datalainnya" id="data-lain">Data Lainnya</a></li>
          </ul>
        </li>
      <?php } ?>
      <?php if($this->session->userdata('status')==9){ ?>
        <li> <a href="#pages-nav-admin" id="admin" data-toggle="collapse" aria-expanded="false"><i class="icon-home"></i><span>Panel Admin</span>
            <div class="arrow pull-right"><i class="fa fa-angle-down"></i></div></a>
          <ul id="pages-nav-admin" class="collapse list-unstyled">
            <li> <a href="<?=base_url()?>data/datapengguna" id="kelola-pengguna">Kelola Pengguna</a></li>
            <li> <a href="<?=base_url()?>data/importdata" id="import-data">Import Data</a></li>
          </ul>
        </li>
      <?php } ?>
      </ul>
    </div>
  </div>
</nav>
<script type="text/javascript" src="<?=base_url()?>bootstrap/js/bukulink.js"></script>
<script type="text/javascript">
  function siderControl(content){
    if(content=='dashboard'){
      $('#pages-nav-home').removeClass('collapse');
      $('#pages-nav-dataanggota').addClass('collapse');
      $('#pages-nav-admin').addClass('collapse');
      $('#home').attr('aria-expanded','true');
      $('#data').attr('aria-expanded','false');
      $('#admin').attr('aria-expanded','false');
      $('#sider-dashboard').addClass('collapse-active');
      $('#sider-editprofil').removeClass('collapse-active');
    }else if(content=='editprofile'){
      $('#pages-nav-home').removeClass('collapse');
      $('#pages-nav-dataanggota').addClass('collapse');
      $('#pages-nav-admin').addClass('collapse');
      $('#home').attr('aria-expanded','true');
      $('#data').attr('aria-expanded','false');
      $('#admin').attr('aria-expanded','false');
      $('#sider-dashboard').removeClass('collapse-active');
      $('#sider-editprofil').addClass('collapse-active');
    }else if(content=="data_lengkap"){
      $('#pages-nav-home').addClass('collapse');
      $('#pages-nav-dataanggota').removeClass('collapse');
      $('#pages-nav-admin').addClass('collapse');
      $('#home').attr('aria-expanded','false');
      $('#data').attr('aria-expanded','true');
      $('#admin').attr('aria-expanded','false');
      $('#data-lengkap').addClass('collapse-active');
      $('#data-alumni').removeClass('collapse-active');
      $('#data-dosen').removeClass('collapse-active');
      $('#data-lain').removeClass('collapse-active');
      $('#data-mahasiswa').removeClass('collapse-active');
    }else if(content=="data_alumni"){
      $('#pages-nav-home').addClass('collapse');
      $('#pages-nav-dataanggota').removeClass('collapse');
      $('#pages-nav-admin').addClass('collapse');
      $('#home').attr('aria-expanded','false');
      $('#data').attr('aria-expanded','true');
      $('#admin').attr('aria-expanded','false');
      $('#data-lengkap').removeClass('collapse-active');
      $('#data-alumni').addClass('collapse-active');
      $('#data-dosen').removeClass('collapse-active');
      $('#data-lain').removeClass('collapse-active');
      $('#data-mahasiswa').removeClass('collapse-active');
    }else if(content=="data_dosen"){
      $('#pages-nav-home').addClass('collapse');
      $('#pages-nav-dataanggota').removeClass('collapse');
      $('#pages-nav-admin').addClass('collapse');
      $('#home').attr('aria-expanded','false');
      $('#data').attr('aria-expanded','true');
      $('#admin').attr('aria-expanded','false');
      $('#data-lengkap').removeClass('collapse-active');
      $('#data-alumni').removeClass('collapse-active');
      $('#data-dosen').addClass('collapse-active');
      $('#data-lain').removeClass('collapse-active');
      $('#data-mahasiswa').removeClass('collapse-active');
    }else if(content=="data_lainnya"){
      $('#pages-nav-home').addClass('collapse');
      $('#pages-nav-dataanggota').removeClass('collapse');
      $('#pages-nav-admin').addClass('collapse');
      $('#home').attr('aria-expanded','false');
      $('#data').attr('aria-expanded','true');
      $('#admin').attr('aria-expanded','false');
      $('#data-lengkap').removeClass('collapse-active');
      $('#data-alumni').removeClass('collapse-active');
      $('#data-dosen').removeClass('collapse-active');
      $('#data-lain').addClass('collapse-active');
      $('#data-mahasiswa').removeClass('collapse-active');
    }else if(content=="data_mahasiswa"){
      $('#pages-nav-home').addClass('collapse');
      $('#pages-nav-dataanggota').removeClass('collapse');
      $('#pages-nav-admin').addClass('collapse');
      $('#home').attr('aria-expanded','false');
      $('#data').attr('aria-expanded','true');
      $('#admin').attr('aria-expanded','false');
      $('#data-lengkap').removeClass('collapse-active');
      $('#data-alumni').removeClass('collapse-active');
      $('#data-dosen').removeClass('collapse-active');
      $('#data-lain').removeClass('collapse-active');
      $('#data-mahasiswa').addClass('collapse-active');
    }else if (content=='admin/import_data'){
      $('#pages-nav-home').addClass('collapse');
      $('#pages-nav-dataanggota').addClass('collapse');
      $('#pages-nav-admin').removeClass('collapse');
      $('#home').attr('aria-expanded','false');
      $('#data').attr('aria-expanded','false');
      $('#admin').attr('aria-expanded','true');
      $('#import-data').addClass('collapse-active');
      $('#kelola-pengguna').removeClass('collapse-active');
    }else if(content=="admin/kelola_pengguna"){
      $('#pages-nav-home').addClass('collapse');
      $('#pages-nav-dataanggota').addClass('collapse');
      $('#pages-nav-admin').removeClass('collapse');
      $('#home').attr('aria-expanded','false');
      $('#data').attr('aria-expanded','false');
      $('#admin').attr('aria-expanded','true');
      $('#import-data').removeClass('collapse-active');
      $('#kelola-pengguna').addClass('collapse-active');
    }
  }
  $(document).ready(function(){
    siderControl('<?=$content?>');
  });
</script>
