function terima(id){
  $.ajax({
    method:"post",
    url:"datapengguna/terima",
    data:{noinduk:id},
    dataType:"json",
    success:function(data){
      if(data.status){
        location.reload();
      }
    }
  });
}

  function batal(id){
    $.ajax({
      method:"post",
      url:"datapengguna/batal",
      data:{noinduk:id},
      dataType:"json",
      success:function(data){
        if(data.status){
          location.reload();
        }
      }
    });
  }
    function hapus(id){
      $.ajax({
        method:"post",
        url:"datapengguna/hapus",
        data:{noinduk:id},
        dataType:"json",
        success:function(data){
          if(data.status){
            location.reload();
          }
        }
      });
    }

    function tambahAdmin(id){
      $.ajax({
        method:"post",
        url:"datapengguna/jadikanadmin",
        data:{noinduk:id},
        dataType:"json",
        success:function(data){
          if(data.status){
            location.reload();
          }
        }
      });
    }

    function detail(id){
      var noid=id;
      $.ajax({
        method:"post",
        url:"datapengguna/fetchdata",
        data:{noinduk:noid},
        dataType:"json",
        success:function(data){
          if(data.data!=false){
            // swal(data['data']);
            // $('[name="detail-nama"]').each(function(){
            //   $(this).html(data.data.nama);
            // });
            // $('[name="detail-noinduk"]').each(function(){
            //   $(this).html(data.data.no_induk);
            // });
            // $('[name="detail-instansi"]').each(function(){
            //   $(this).html(data.data.instansi);
            // });
            // $('[name="detail-alamat"]').each(function(){
            //   $(this).html(data.data.alamat);
            // });
            // $('[name="detail-email"]').each(function(){
            //   $(this).html(data.data.email);
            // });
            // $('[name="detail-status"]').each(function(){
            //   $(this).html(data.status.nama);
            // });
            $('#detail-nama').html(data.data.nama);
            $('#detail-noinduk').html(data.data.no_induk);
            $('#detail-instansi').html(data.data.instansi);
            $('#detail-alamat').html(data.data.alamat);
            $('#detail-email').html(data.data.email);
            $('#detail-status').html(data.status.nama);
            $('#detail-telepon').html(data.data.telepon);
            $('#detail-pengguna').modal({'show':true});
          }
        }
      });
    }
