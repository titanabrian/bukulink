<!-- Side Navbar -->
<nav class="side-navbar">
  <div class="side-navbar-wrapper">
    <div class="sidenav-header d-flex align-items-center justify-content-center">
      <div class="sidenav-header-inner text-center">
        <h2 class="h5 text-uppercase">Anderson Hardy</h2><span class="text-uppercase">Web Developer</span>
      </div>
      <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>B</strong><strong class="text-primary">L</strong></a></div>
    </div>
    <!-- <div class="main-menu">
      <ul id="side-main-menu" class="side-menu list-unstyled">
        <li class="active"><a href="index.html"> <i class="icon-home"></i><span>Home</span></a></li>
        <li> <a href="forms.html"><i class="icon-form"></i><span>Forms</span></a></li>
        <li> <a href="charts.html"><i class="icon-presentation"></i><span>Charts</span></a></li>
        <li> <a href="tables.html"> <i class="icon-grid"> </i><span>Tables  </span></a></li>
        <li> <a href="login.html"> <i class="icon-interface-windows"></i><span>Login page                        </span></a></li>
        <li> <a href="#"> <i class="icon-mail"></i><span>Demo</span>
            <div class="badge badge-warning">6 New</div></a></li>
      </ul>
    </div> -->
    <div class="admin-menu">
      <ul id="side-admin-menu" class="side-menu list-unstyled">
        <li> <a href="#pages-nav-home" data-toggle="collapse" aria-expanded="false"><i class="icon-home"></i><span>Home</span>
            <div class="arrow pull-right"><i class="fa fa-angle-down"></i></div></a>
          <ul id="pages-nav-home" class="collapse list-unstyled">
            <li> <a href="#">Dashboard</a></li>
            <li> <a href="#">Profil</a></li>
          </ul>
        </li>
        <li> <a href="#pages-nav-dataanggota" data-toggle="collapse" aria-expanded="false"><i class="icon-interface-windows"></i><span>Data Anggota</span>
            <div class="arrow pull-right"><i class="fa fa-angle-down"></i></div></a>
          <ul id="pages-nav-dataanggota" class="collapse list-unstyled">
            <li> <a href="#">Data Lengkap</a></li>
            <li> <a href="#">Data Mahasiswa</a></li>
            <li> <a href="#">Data Alumni</a></li>
            <li> <a href="#">Data Dosen</a></li>
            <li> <a href="#">Data Eksternal</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
